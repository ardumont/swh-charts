{{ if .Values.loaders.enabled -}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: loader-utils
  namespace: {{ $.Values.namespace }}
data:
  pre-stop-idempotent.sh: |
    #!/bin/bash

    # pre-stop hook can be triggered multiple times but we want it to be applied only
    # once so container can warm-shutdown properly.

    # When celery receives multiple times the sigterm signal, this ends up doing an
    # immediate shutdown which prevents long-standing tasks to finish properly.

    set -ex

    WITNESS_FILE=/tmp/already-stopped

    # Seed awk with the number of nanoseconds since epoch
    # and have it generate a number between 0 and 1
    sleep $(date +%s%N | awk '{srand($1); print rand()}')

    if [ ! -e $WITNESS_FILE ]; then
      touch $WITNESS_FILE
      kill 1
    fi

{{ range $loader_type, $deployment_config := .Values.loaders.deployments }}
{{ $loader_name := ( print "loader-" $loader_type ) }}
{{ $log_level := get $deployment_config "logLevel" | default "INFO" | quote }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ $loader_name }}-template
  namespace: {{ $.Values.namespace }}
data:
  config.yml.template: |
    storage:
      cls: pipeline
      steps:
      - cls: buffer
        min_batch_size:
          content: 1000
          content_bytes: 52428800
          directory: 1000
          directory_entries: 12000
          revision: 1000
          revision_parents: 2000
          revision_bytes: 52428800
          release: 1000
          release_bytes: 52428800
          extid: 1000
      - cls: filter
      - cls: retry
      - cls: remote
        url: http://{{ $.Values.loaders.storage.host }}:{{ $.Values.loaders.storage.port }}/

    {{- if $deployment_config.extraConfig -}}
      {{- range $option, $value := $deployment_config.extraConfig }}
    {{ $option }}: {{ toYaml $value | nindent 6 }}
      {{- end }}
    {{- end }}

    celery:
      task_broker: amqp://${AMQP_USERNAME}:${AMQP_PASSWORD}@{{ $.Values.loaders.amqp.host }}:{{ $.Values.loaders.amqp.port }}/
      task_acks_late: {{ get $deployment_config "ackLate" | default false }}
      task_queues:
    {{- range $queue := get $deployment_config "queues" }}
      - {{ $queue }}
    {{- end }}
    metadata_fetcher_credentials:
  init-container-entrypoint.sh: |
    #!/bin/bash

    set -e

    CONFIG_FILE=/etc/swh/config.yml

    # substitute environment variables when creating the default config.yml
    eval echo \""$(</etc/swh/configuration-template/config.yml.template)"\" \
      > $CONFIG_FILE

    CREDS_LISTER_PATH=/etc/credentials/metadata-fetcher/credentials
    [ -f $CREDS_LISTER_PATH ] && \
      sed 's/^/  /g' $CREDS_LISTER_PATH >> $CONFIG_FILE

    exit 0
  logging-configuration.yml: |
    version: 1

    handlers:
      console:
        class: logging.StreamHandler
        formatter: json
        stream: ext://sys.stdout

    formatters:
      json:
        class: pythonjsonlogger.jsonlogger.JsonFormatter
        # python-json-logger parses the format argument to get the variables it actually expands into the json
        format: "%(asctime)s:%(threadName)s:%(pathname)s:%(lineno)s:%(funcName)s:%(task_name)s:%(task_id)s:%(name)s:%(levelname)s:%(message)s"

    loggers:
      celery:
        level: {{ $log_level }}
      amqp:
        level: WARNING
      urllib3:
        level: WARNING
      azure.core.pipeline.policies.http_logging_policy:
        level: WARNING
      swh:
        level: {{ $log_level }}
      celery.task:
        level: {{ $log_level }}

    root:
      level: {{ $log_level }}
      handlers:
      - console

{{ end }}
{{- end -}}
