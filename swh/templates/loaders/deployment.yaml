{{ if .Values.loaders.enabled -}}
{{- $configurationChecksum := include (print $.Template.BasePath "/loaders/configmap.yaml") . -}}
{{- range $loader_type, $deployment_config := .Values.loaders.deployments -}}
{{- $loader_name := ( print "loader-" $loader_type ) -}}
# if defined at the "typed" loader level
{{- $local_container_image_key := get $deployment_config "image" }}
{{- $local_container_image := get $.Values $local_container_image_key }}
{{- $local_container_image_version_key := ( print $local_container_image_key "_version" ) }}
{{- $local_container_image_version := get $.Values $local_container_image_version_key }}
# otherwise if the global image is defined
{{- $image_name_key := ( print "swh_loader_" $loader_type "_image" ) -}}
{{- $image_version_key := ( print $image_name_key "_version" ) -}}
{{- $container_image := get $.Values $image_name_key }}
{{- $container_image_version := get $.Values $image_version_key }}
{{- $loader_affinity := get $deployment_config "affinity" | default $.Values.loaders.affinity }}
# TODO: Temporary ack to remove when logging is properly routed to logstash
{{- $use_json_logger := get $deployment_config "useJsonLogger" | default false }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $loader_name }}
  namespace: {{ $.Values.namespace }}
  labels:
    app: {{ $loader_name }}
spec:
  revisionHistoryLimit: 2
  selector:
    matchLabels:
      app: {{ $loader_name }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
  template:
    metadata:
      labels:
        app: {{ $loader_name }}
      annotations:
        # Force a rollout upgrade if the configuration changes
        checksum/config: {{ $configurationChecksum | sha256sum }}
    spec:
      {{- if $loader_affinity }}
      affinity:
        {{- toYaml $loader_affinity | nindent 8 }}
      {{- end }}
      terminationGracePeriodSeconds: 3600
      initContainers:
        - name: prepare-configuration
          image: debian:bullseye
          imagePullPolicy: Always
          env:
          - name: AMQP_USERNAME
            valueFrom:
              secretKeyRef:
                name: common-secrets
                key: rabbitmq-amqp-username
                # 'name' secret must exist & include that ^ key
                optional: false
          - name: AMQP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: common-secrets
                key: rabbitmq-amqp-password
                # 'name' secret must exist & include that ^ key
                optional: false
          {{ if ( contains "deposit" $loader_name ) -}}
          - name: DEPOSIT_USERNAME
            valueFrom:
              secretKeyRef:
                name: common-secrets
                key: deposit-username
                # 'name' secret must exist & include key "host"
                optional: false
          - name: DEPOSIT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: common-secrets
                key: deposit-password
                # 'name' secret must exist & include key "host"
                optional: false
          {{ end }}
          command:
            - /entrypoint.sh
          volumeMounts:
          - name: configuration-template
            mountPath: /entrypoint.sh
            subPath: "init-container-entrypoint.sh"
            readOnly: true
          - name: configuration
            mountPath: /etc/swh
          - name: configuration-template
            mountPath: /etc/swh/configuration-template
          - name: metadata-fetcher-credentials
            mountPath: /etc/credentials/metadata-fetcher
            readOnly: true
      containers:
      - name: loaders
        {{ if $local_container_image -}}
        image: {{ $local_container_image }}:{{ $local_container_image_version }}
        {{ else if $container_image -}}
        image: {{ $container_image }}:{{ $container_image_version }}
        {{ end }}
        imagePullPolicy: Always
        command:
          - /opt/swh/entrypoint.sh
        resources:
          requests:
            memory: {{ get $deployment_config "requestedMemory" | default "512Mi" }}
            cpu: {{ get $deployment_config "requestedCpu" | default "500m" }}
        lifecycle:
          preStop:
            exec:
              command: ["/pre-stop.sh"]
        env:
        - name: STATSD_HOST
          value: {{ $.Values.statsdExternalHost | default "prometheus-statsd-exporter" }}
        - name: STATSD_PORT
          value: {{ $.Values.statsdPort | default "9125" | quote }}
        - name: MAX_TASKS_PER_CHILD
          value: {{ get $deployment_config "maxTasksPerChild" | default 10 | quote }}
        - name: LOGLEVEL
          value: {{ get $deployment_config "logLevel" | default "INFO" | quote }}
        - name: SWH_CONFIG_FILENAME
          value: /etc/swh/config.yml
        {{ if $use_json_logger -}}
        - name: SWH_LOG_CONFIG
          value: /etc/swh/logging-configuration.yml
        {{ end }}
        - name: SWH_SENTRY_ENVIRONMENT
          value: {{ $.Values.sentry.environment }}
        - name: SWH_MAIN_PACKAGE
          value: {{ get $deployment_config "sentrySwhPackage" }}
        - name: SWH_SENTRY_DSN
          valueFrom:
            secretKeyRef:
              name: common-secrets
              key: {{ $loader_name}}-sentry-dsn
              # 'name' secret must exist & include key "host"
              optional: false
        - name: SWH_SENTRY_DISABLE_LOGGING_EVENTS
          value: {{ get $deployment_config "sentryDisableLoggingEvents" | default "yes" | quote }}
        volumeMounts:
          - name: loader-utils
            mountPath: /pre-stop.sh
            subPath: "pre-stop.sh"
          - name: configuration
            mountPath: /etc/swh
          - name: localstorage
            mountPath: /tmp
          - name: configuration-template
            mountPath: /etc/swh/logging-configuration.yml
            subPath: "logging-configuration.yml"
            readOnly: true
      volumes:
      - name: localstorage
{{ toYaml ($.Values.tmpEphemeralStorage.claimTemplate | default $.Values.tmpEphemeralStorage.default) | indent 8 }}
      - name: configuration
        emptyDir: {}
      - name: configuration-template
        configMap:
          name: {{ $loader_name }}-template
          defaultMode: 0777
          items:
          - key: "config.yml.template"
            path: "config.yml.template"
          - key: "init-container-entrypoint.sh"
            path: "init-container-entrypoint.sh"
          - key: "logging-configuration.yml"
            path: "logging-configuration.yml"
      - name: loader-utils
        configMap:
          name: loader-utils
          defaultMode: 0777
          items:
          - key: "pre-stop-idempotent.sh"
            path: "pre-stop.sh"
      - name: metadata-fetcher-credentials
        secret:
          secretName: metadata-fetcher-credentials
          optional: true
{{ end }}
{{- end -}}
